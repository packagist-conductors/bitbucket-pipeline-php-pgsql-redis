FROM php:7.2.24-fpm-alpine
MAINTAINER Nils Adermann <n.adermann@packagist.com>

RUN apk upgrade --no-cache \
    && apk add --no-cache bash ca-certificates curl openssl postgresql-client \
    openssh-client openssl git unzip zip icu \
    postgresql-dev icu-dev zlib-dev \
    python make g++ \
    && apk add  --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/v3.8/main/ nodejs=8.14.0-r0 yarn \
    && echo "Compiled in PHP extensions: " \
    && php -m \
    && docker-php-ext-install intl pgsql pdo_pgsql zip \
    && apk del postgresql-dev icu-dev \
    && ln -s /usr/local/bin/php /usr/bin/php \
    && yarn global add gulp-cli \
    && rm -rf /tmp/* /var/tmp/* \
    && mkdir /tmp/cores

RUN docker-php-source extract \
    && apk add --no-cache --virtual .phpize-deps-configure libtool $PHPIZE_DEPS \
    && pecl install apcu-5.1.8 \
    && docker-php-ext-enable apcu \
    && apk del .phpize-deps-configure \
    && docker-php-source delete

COPY cli-php.ini /usr/local/etc/php/php.ini

RUN printf '\n[ v3_req ]\n\n[ v3_ca ]\n' >> /etc/ssl/openssl.cnf
